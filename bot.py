import os
import re
import telebot
import datetime
from peewee import *

db = SqliteDatabase('sportmuctrbot.db')
token = os.environ['token']
if os.environ['PROXY_IP']:
    telebot.apihelper.proxy = {
        'https': 'socks5://' + os.environ['PROXY_USER'] + ':' + os.environ['PROXY_PASS'] + '@' +
                 os.environ['PROXY_IP'] + ':' + os.environ['PROXY_PORT']
    }
bot = telebot.TeleBot(token)


class BaseModel(Model):
    class Meta:
        database = db


class SportType(BaseModel):
    title = CharField(default='', unique=True)


class Team(BaseModel):
    title = CharField(default='', unique=True)
    sporttype = ForeignKeyField(SportType, default=1)


class Student(BaseModel):
    surname = CharField(default='')
    name = CharField(default='')
    patronymic = CharField(default='')
    faculty = CharField(default='')
    group = CharField(default='')


class Event(BaseModel):
    title = CharField(default='')
    description = TextField(default='')
    sporttype = ForeignKeyField(SportType)
    offsettype = CharField(default='командный зачет')
    date = DateTimeField(default=datetime.datetime.now)
    place = CharField(default='')
    active = BooleanField(default=True)


class StudentTeam(BaseModel):
    student = ForeignKeyField(Student, default=1)
    team = ForeignKeyField(Team, default=1)


class StudentEvent(BaseModel):
    student = ForeignKeyField(Student, default=1)
    event = ForeignKeyField(Event, default=1)
    result = CharField(default='')


class TeamEvent(BaseModel):
    team = ForeignKeyField(Team, default=1)
    event = ForeignKeyField(Event, default=1)
    result = CharField(default='')


class User(BaseModel):
    username = CharField(unique=True)
    student = ForeignKeyField(Student, default=1)
    curr_menu = CharField(default='main')
    curr_news = CharField(default='week')
    curr_events = CharField(default='week')
    curr_event_change = DateTimeField(default=datetime.datetime.now)
    curr_sporttype = CharField(default='')
    curr_team = CharField(default='')
    news_notices = BooleanField(default=True)
    events_notices = BooleanField(default=True)
    admins_notices = BooleanField(default=True)
    login = BooleanField(default=False)
    admin = BooleanField(default=False)
    su = BooleanField(default=False)


class AdminRequest(BaseModel):
    username = CharField(default='')
    nickname = CharField(default='')
    date = DateTimeField(default=datetime.datetime.now)
    active = BooleanField(default=True)
    curr = BooleanField(default=False)


class News(BaseModel):
    title = CharField(default='', unique=True)
    description = TextField(default='')
    date = DateTimeField(default=datetime.datetime.now)
    active = BooleanField(default=True)
    username = CharField()


class Album(BaseModel):
    title = CharField(unique=True)
    description = TextField(default='')
    date = DateTimeField(default=datetime.datetime.now)


class Media(BaseModel):
    title = CharField(unique=True)
    album = ForeignKeyField(Album, default=1)


def generate_markup(username):
    user = User.get(User.username == username)
    curr_menu = user.curr_menu
    list_bts = []
    markup = telebot.types.ReplyKeyboardMarkup(
        row_width=1, resize_keyboard=True)
    if curr_menu == 'main':
        list_bts.append(telebot.types.KeyboardButton('Новости'))
        list_bts.append(telebot.types.KeyboardButton('События'))
        list_bts.append(telebot.types.KeyboardButton('Команды'))
        # list_bts.append(telebot.types.KeyboardButton('Галерея'))
        if user.login:
            list_bts.append(telebot.types.KeyboardButton('Профиль'))
        if user.su:
            list_bts.append(telebot.types.KeyboardButton(
                'Запросы администраторов'))
        list_bts.append(telebot.types.KeyboardButton('Настройка уведомлений'))
        if user.login:
            list_bts.append(telebot.types.KeyboardButton('Выйти'))
        else:
            list_bts.append(telebot.types.KeyboardButton('Авторизоваться'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'news':
        if user.admin:
            list_bts.append(telebot.types.KeyboardButton('Добавить'))
            list_bts.append(telebot.types.KeyboardButton('Удалить'))
        list_bts.append(telebot.types.KeyboardButton('Месяц'))
        list_bts.append(telebot.types.KeyboardButton('Год'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'news_del':
        user = User.get(User.username == username)
        if user.curr_news == 'week':
            delta = datetime.timedelta(days=7)
        elif user.curr_news == 'month':
            delta = datetime.timedelta(days=30)
        elif user.curr_news == 'year':
            delta = datetime.timedelta(days=365)
        begin = datetime.datetime.now()
        end = begin - delta
        news_all = requests = News.select().where(
            News.date <= begin, News.date >= end).order_by(+News.date)
        for news in news_all:
            list_bts.append(telebot.types.KeyboardButton(
                news.date.strftime('%d.%m.%Y %H:%M:%S')))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'events':
        if user.admin:
            list_bts.append(telebot.types.KeyboardButton('Добавить'))
            list_bts.append(telebot.types.KeyboardButton('Изменить'))
            list_bts.append(telebot.types.KeyboardButton('Удалить'))
        list_bts.append(telebot.types.KeyboardButton('Месяц'))
        list_bts.append(telebot.types.KeyboardButton('Год'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu in ('event_del', 'event_change'):
        user = User.get(User.username == username)
        if user.curr_events == 'week':
            delta = datetime.timedelta(days=7)
        elif user.curr_events == 'month':
            delta = datetime.timedelta(days=30)
        elif user.curr_events == 'year':
            delta = datetime.timedelta(days=365)
        begin = datetime.datetime.now() - delta
        end = datetime.datetime.now() + delta
        events = requests = Event.select().where(
            Event.date >= begin, Event.date <= end).order_by(+Event.date)
        for event in events:
            list_bts.append(telebot.types.KeyboardButton(
                event.date.strftime('%d.%m.%Y %H:%M')))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'event_change_type':
        list_bts.append(telebot.types.KeyboardButton('Добавить участника'))
        list_bts.append(telebot.types.KeyboardButton('Удалить участника'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'event_change_del':
        event = Event.get(Event.date == user.curr_event_change)
        if event.offsettype == 'командный зачет':
            teams = (Team
                     .select()
                     .join(TeamEvent)
                     .where(TeamEvent.event == event))
            for team in teams:
                list_bts.append(telebot.types.KeyboardButton(team.title))
        if event.offsettype == 'личный зачет':
            students = (Student
                        .select()
                        .join(StudentEvent)
                        .where(StudentEvent.event == event))
            for student in members:
                list_bts.append(telebot.types.KeyboardButton(student.surname.capitalize(
                ) + ' ' + student.name.capitalize() + ' ' + student.group.capitalize()))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'sport_types':
        sport_types = SportType.select().order_by(SportType.title)
        for sport_type in sport_types:
            list_bts.append(telebot.types.KeyboardButton(
                sport_type.title.capitalize()))
        if user.admin:
            list_bts.append(telebot.types.KeyboardButton('Добавить'))
            list_bts.append(telebot.types.KeyboardButton('Удалить'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'sport_type_del':
        sport_types = SportType.select().order_by(SportType.title)
        for sport_type in sport_types:
            list_bts.append(telebot.types.KeyboardButton(
                sport_type.title.capitalize()))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'teams':
        if user.admin:
            list_bts.append(telebot.types.KeyboardButton('Добавить'))
            list_bts.append(telebot.types.KeyboardButton('Изменить'))
            list_bts.append(telebot.types.KeyboardButton('Удалить'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu in ('team_del', 'team_change'):
        user = User.get(User.username == username)
        sport_type = SportType.get(SportType.title == user.curr_sporttype)
        teams = Team.select().where(Team.sporttype == sport_type)
        for team in teams:
            btn_name = team.title.capitalize()
            list_bts.append(telebot.types.KeyboardButton(btn_name))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'team_change_type':
        list_bts.append(telebot.types.KeyboardButton('Название'))
        list_bts.append(telebot.types.KeyboardButton('Состав'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'team_change_member':
        list_bts.append(telebot.types.KeyboardButton('Добавить'))
        list_bts.append(telebot.types.KeyboardButton('Удалить'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'team_change_member_del':
        students = (Student
                    .select()
                    .join(StudentTeam)
                    .join(Team)
                    .where(Team.title == user.curr_team))
        for student in students:
            list_bts.append(telebot.types.KeyboardButton(student.surname.capitalize(
            ) + ' ' + student.name.capitalize() + ' ' + student.group.capitalize()))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'notices':
        list_bts.append(telebot.types.KeyboardButton('Новости'))
        list_bts.append(telebot.types.KeyboardButton('События'))
        if user.su:
            list_bts.append(telebot.types.KeyboardButton(
                'Запросы администраторов'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'admin_notice':
        list_bts.append(telebot.types.KeyboardButton('Утвердить'))
        list_bts.append(telebot.types.KeyboardButton('Отклонить'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup
    elif curr_menu == 'admin_notices':
        requests = AdminRequest.select().where(
            AdminRequest.active == True).order_by(-AdminRequest.date)
        for request in requests:
            if request.nickname != '':
                btn_name = '@' + request.nickname
            else:
                btn_name = request.username
            list_bts.append(telebot.types.KeyboardButton(btn_name))
        list_bts.append(telebot.types.KeyboardButton('Утвердить все'))
        list_bts.append(telebot.types.KeyboardButton('Отклонить все'))
        list_bts.append(telebot.types.KeyboardButton('Отмена'))
        markup.add(*list_bts)
        return markup


def show_admin_requests():
    user = User.get(User.su == True)
    try:
        requests = [AdminRequest.get(AdminRequest.curr == True)]
    except:
        requests = AdminRequest.select().where(
            AdminRequest.active == True).order_by(+AdminRequest.date)
    if len(requests) == 0:
        user.curr_menu = 'main'
        user.save()
        markup = generate_markup(user.username)
        bot.send_message(
            int(user.username), 'У Вас нет активных запросов.', reply_markup=markup)
        return
    markup = generate_markup(user.username)
    for request in requests:
        notice = 'Вам поступил новый запрос на повышение привелегий:\n\n' + \
                 'ID: ' + request.username + '\n'
        if request.nickname != '':
            notice += 'UserName: ' + request.nickname + '\n'
        notice += 'Дата: ' + request.date.strftime('%d.%m.%Y %H:%M:%S')
        bot.send_message(int(user.username), notice, reply_markup=markup)


def admin_notice(message):
    username = str(message.chat.id)
    markup = generate_markup(username)
    if message.text.lower() in ('утвердить все', 'утвердить'):
        request = AdminRequest.get(AdminRequest.curr == True)
        request_user = User.update(admin=True).where(
            User.username == request.username)
        request_user.execute()
        request.active = False
        request.curr = False
        request.save()
        if request.nickname != '':
            user = request.nickname
        else:
            user = request.username
        bot.send_message(message.chat.id, 'Запрос пользователя ' +
                         user + ' успешно утвержден.', reply_markup=markup)
    elif message.text.lower() in ('отклонить все', 'отклонить'):
        request = AdminRequest.get(AdminRequest.curr == True)
        request.active = False
        request.curr = False
        request.save()
        if request.nickname != '':
            user = request.nickname
        else:
            user = request.username
        bot.send_message(message.chat.id, 'Запрос пользователя ' +
                         user + ' успешно отклонен.', reply_markup=markup)
    elif message.text.lower() == 'отмена':
        request = AdminRequest.update(
            curr=False).where(AdminRequest.curr == True)
        request.execute()
        bot.send_message(
            message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
    else:
        bot.send_message(
            message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')


@bot.message_handler(commands=['start'])
def handle_command_start(message):
    username = str(message.chat.id)
    su = False
    admin = False
    if username == os.environ['su_id']:
        su = True
        admin = True
    user = User.get_or_create(
        username=username,
        defaults={
            'su': su,
            'admin': admin
        }
    )
    user = user[0]
    if user.curr_menu != 'main':
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    markup = generate_markup(username)
    if user.login:
        name = user.student.name.capitalize()
        patronymic = user.student.patronymic.capitalize()
        if patronymic != '':
            patronymic = ' ' + patronymic
        bot.send_message(message.chat.id, 'Добро пожаловать, ' +
                         name + patronymic + '!', reply_markup=markup)
    else:
        bot.send_message(message.chat.id, 'Добро пожаловать!\n' +
                         'Авторизуйтесь, чтобы следить за своими спортивными достижениями!\n', reply_markup=markup)


def handle_command_news(message):
    username = str(message.chat.id)
    user = User.get(User.username == username)
    if user.curr_menu not in ('main', 'news'):
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    user.curr_menu = 'news'
    user.save()
    markup = generate_markup(username)
    if user.curr_news == 'week':
        delta = datetime.timedelta(days=7)
    elif user.curr_news == 'month':
        delta = datetime.timedelta(days=30)
    elif user.curr_news == 'year':
        delta = datetime.timedelta(days=365)
    begin = datetime.datetime.now()
    end = begin - delta
    news_all = News.select().where(
        News.date <= begin, News.date >= end).order_by(+News.date)
    if len(news_all) != 0:
        for news in news_all:
            bot.send_message(message.chat.id, news.title +
                             '\n\n' + news.description + '\n\n' + news.date.strftime('%d.%m.%Y %H:%M:%S'),
                             reply_markup=markup)
    else:
        bot.send_message(message.chat.id, 'Новостей пока нет.',
                         reply_markup=markup)


def handle_command_gallery(message):
    pass


def handle_command_events(message):
    username = str(message.chat.id)
    user = User.get(User.username == username)
    user.curr_menu = 'events'
    user.save()
    markup = generate_markup(username)
    if user.curr_events == 'week':
        delta = datetime.timedelta(days=7)
    elif user.curr_events == 'month':
        delta = datetime.timedelta(days=30)
    elif user.curr_events == 'year':
        delta = datetime.timedelta(days=365)
    begin = datetime.datetime.now() - delta
    end = datetime.datetime.now() + delta
    events = Event.select().where(
        Event.date >= begin, Event.date <= end).order_by(+Event.date)
    if len(events) != 0:
        for event in events:
            members_str = ''
            if event.offsettype == 'командный зачет':
                teams = (Team
                         .select()
                         .join(TeamEvent)
                         .where(TeamEvent.event == event))
                for team in teams:
                    teamevent = TeamEvent.get(
                        TeamEvent.event == event, TeamEvent.team == team)
                    members_str += team.title + ': ' + teamevent.result + '\n'
            else:
                students = (Student
                            .select()
                            .join(StudentEvent)
                            .where(StudentEvent.event == event))
                for student in students:
                    studentevent = StudentEvent.get(
                        StudentEvent.event == event, StudentEvent.student == student)
                    members_str += student.surname.capitalize() + ' ' + student.name.capitalize() + \
                                   ' ' + student.group.upper() + ': ' + studentevent.result
            if members_str == '':
                members_str = 'Еще не объявлены.'
            bot.send_message(message.chat.id, event.title + '\n' +
                             event.place + '\n' +
                             event.sporttype.title.capitalize() + '\n' +
                             event.offsettype.capitalize() + '\n' +
                             event.date.strftime('%d.%m.%Y %H:%M:%S') + '\n\n' +
                             event.description + '\n\n' +
                             'Участники:\n\n' +
                             members_str,
                             reply_markup=markup
                             )
    else:
        bot.send_message(message.chat.id, 'Событий пока нет.',
                         reply_markup=markup)


def handle_command_notices(message):
    username = str(message.chat.id)
    user = User.get(User.username == username)
    if user.curr_menu != 'main':
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    user.curr_menu = 'notices'
    user.save()
    news_notices = str(user.news_notices)
    events_notices = str(user.events_notices)
    admins_notices = str(user.admins_notices)
    if user.su:
        notice = 'Ваши текущие настройки уведомлений:\n\n' + \
                 'Новости: ' + news_notices + '\n' + \
                 'Предстоящие события: ' + events_notices + '\n' + \
                 'Запросы администраторов: ' + admins_notices + '\n\n' + \
                 'Что Вы хотите изменить?'
    else:
        notice = 'Ваши текущие настройки уведомлений:\n\n' + \
                 'Новости: ' + news_notices + '\n' + \
                 'Предстоящие события: ' + events_notices + '\n\n' + \
                 'Что Вы хотите изменить?'
    markup = generate_markup(username)
    bot.send_message(message.chat.id, notice, reply_markup=markup)


@bot.message_handler(commands=['admin'])
def handle_command_admin(message):
    date = datetime.datetime.now()
    username = str(message.chat.id)
    user_sender = User.get(User.username == username)
    if user_sender.curr_menu != 'main':
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    if user_sender.admin:
        bot.send_message(message.chat.id, 'Вы уже являетесь администратором!')
        return
    try:
        nickname = '@' + message.chat.username
    except:
        nickname = ''
    markup = generate_markup(username)
    bot.send_message(
        message.chat.id, 'Ваш запрос успешно отправлен!\nОжидайте подтверждения.', reply_markup=markup)
    user = User.get(User.su == True)
    if user.admins_notices and user.curr_menu == 'main':
        request = AdminRequest.create(
            username=username, nickname=nickname, date=date, curr=True)
        user.curr_menu = 'admin_notice'
        user.save()
        show_admin_requests()
    else:
        request = AdminRequest.create(
            username=username, nickname=nickname, date=date)


def handle_command_login(message):
    username = str(message.chat.id)
    user = User.get(User.username == username)
    if user.curr_menu != 'main':
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    if user.login:
        name = user.student.name.capitalize()
        patronymic = user.student.patronymic.capitalize()
        if patronymic != '':
            patronymic = ' ' + patronymic
        bot.send_message(message.chat.id, name +
                         patronymic + ', Вы уже авторизованы!')
        return
    user.curr_menu = 'login'
    user.save()
    markup = telebot.types.ReplyKeyboardRemove()
    bot.send_message(message.chat.id, 'Веведите Ваше ФИО, факультет и группу. ' +
                     'При отсутвии какого-либо пункта оставьте строку пустой. ' +
                     'Введите Отмена для выхода в главное меню.\n\n' +
                     'Например:\n' +
                     'Иванов\n' +
                     'Иван\n' +
                     'Иванович\n' +
                     'ИТУ\n' +
                     'Кс-10',
                     reply_markup=markup
                     )


def handle_command_logout(message):
    username = str(message.chat.id)
    user = User.get(User.username == username)
    if user.curr_menu != 'main':
        bot.send_message(
            message.chat.id, 'Пожалуйста, завершите текущее действие!')
        return
    if not user.login:
        bot.send_message(message.chat.id, 'Вы еще не авторизованы!')
        return
    name = user.student.name.capitalize()
    patronymic = user.student.patronymic.capitalize()
    if patronymic != '':
        patronymic = ' ' + patronymic
    user.student = 1
    user.login = False
    user.save()
    markup = generate_markup(username)
    bot.send_message(message.chat.id, name + patronymic +
                     ', Вы отсоединили текущий аккаунт!\n' +
                     'Теперь Вам не будет доступна статистика Ваших спортивных достижений.',
                     reply_markup=markup
                     )


@bot.message_handler(content_types=['text'])
def handle_text(message):
    username = str(message.chat.id)
    user_sender = User.get(User.username == username)
    curr_menu = user_sender.curr_menu
    if curr_menu == 'main':
        if message.text.lower() == 'новости':
            handle_command_news(message)
        elif message.text.lower() == 'события':
            handle_command_events(message)
        elif message.text.lower() == 'команды':
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете вид спорта.', reply_markup=markup)
        elif message.text.lower() == 'профиль':
            if not user_sender.login:
                bot.send_message(message.chat.id, 'Вы еще не авторизованы!')
                return
            teams = (Team
                     .select()
                     .join(StudentTeam)
                     .where(StudentTeam.student == user_sender.student))
            events = (Event
                      .select()
                      .join(StudentEvent)
                      .where(StudentEvent.student == user_sender.student))
            teams_str = ''
            events_str = ''
            if len(teams) == 0:
                teams_str = 'Вы пока не числитесь ни в одной команде.\n'
            if len(events) == 0:
                events_str = 'Вы пока не учавствовали в спортивных событиях.\n'
            for team in teams:
                teams_str += team.title + '\n'
            teams_str += '\n'
            for event in events:
                studentevent = StudentEvent.get(
                    event=event, student=user_sender.student)
                events_str += event.title + '\n' + \
                              event.date.strftime('%d.%m.%Y %H:%M:%S') + \
                              '\n' + studentevent.result + '\n\n'
            bot.send_message(message.chat.id, 'Участие в комадах:\n' + teams_str +
                             'Личные результаты:\n\n' + events_str)
        elif message.text.lower() == 'настройка уведомлений':
            handle_command_notices(message)
        elif message.text.lower() == 'запросы администраторов' and user_sender.su:
            user_sender.curr_menu = 'admin_notices'
            user_sender.save()
            show_admin_requests()
        elif message.text.lower() == 'авторизоваться':
            handle_command_login(message)
        elif message.text.lower() == 'выйти':
            handle_command_logout(message)
        else:
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.', reply_markup=markup)
            return
        return
    if curr_menu == 'events':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.curr_events = 'week'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
        elif message.text.lower() == 'месяц':
            user_sender.curr_events = 'month'
            user_sender.save()
            handle_command_events(message)
        elif message.text.lower() == 'год':
            user_sender.curr_events = 'year'
            user_sender.save()
            handle_command_events(message)
        elif message.text.lower() == 'добавить' and user_sender.admin:
            user_sender.curr_menu = 'event_add'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id,
                             'Добавьте названеие, место, вид спорта, тип зачета (Командный зачет/Личный зачет), время (12.12.2012 12:12) и описание события. Введите Отмена для выхода в главное меню.\n\n' +
                             'Например:\n' +
                             'Турнир по волейболу.\n' +
                             'РХТУ 2\n' +
                             'Волейбол\n' +
                             'Командный зачет\n' +
                             '12.12.2012 12:12\n' +
                             'Описание_события',
                             reply_markup=markup
                             )
        elif message.text.lower() == 'удалить' and user_sender.admin:
            if user_sender.curr_events == 'week':
                delta = datetime.timedelta(days=7)
            elif user_sender.curr_events == 'month':
                delta = datetime.timedelta(days=30)
            elif user_sender.curr_events == 'year':
                delta = datetime.timedelta(days=365)
            begin = datetime.datetime.now() - delta
            end = datetime.datetime.now() + delta
            events = Event.select().where(
                Event.date >= begin, Event.date <= end).order_by(+Event.date)
            if len(events) == 0:
                bot.send_message(message.chat.id, 'Событий пока нет.')
                return
            user_sender.curr_menu = 'event_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете событие из списка выше, которое хотие удалить.', reply_markup=markup)
        elif message.text.lower() == 'изменить' and user_sender.admin:
            if user_sender.curr_events == 'week':
                delta = datetime.timedelta(days=7)
            elif user_sender.curr_events == 'month':
                delta = datetime.timedelta(days=30)
            elif user_sender.curr_events == 'year':
                delta = datetime.timedelta(days=365)
            begin = datetime.datetime.now() - delta
            end = datetime.datetime.now() + delta
            events = Event.select().where(
                Event.date >= begin, Event.date <= end).order_by(+Event.date)
            if len(events) == 0:
                bot.send_message(message.chat.id, 'Событий пока нет.')
                return
            user_sender.curr_menu = 'event_change'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете событие из списка выше, которое хотие изменить.', reply_markup=markup)
    if curr_menu == 'event_add':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        data = re.split('\n', message.text)
        if len(data) != 6:
            print(1)
            bot.send_message(
                message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
            return
        try:
            date = datetime.datetime.strptime(data[4], '%d.%m.%Y %H:%M')
        except:
            bot.send_message(
                message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
            return
        try:
            event = Event.get(Event.date == date)
            bot.send_message(
                message.chat.id, 'Время события должно быть уникальным!')
            return
        except:
            if data[3].lower() not in ('командный зачет', 'личный зачет'):
                print(3)
                bot.send_message(
                    message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
                return
            sporttype = SportType.get_or_create(title=data[2].lower())
            event_create = Event.create(title=data[0], place=data[1], sporttype=sporttype[
                0], offsettype=data[3].lower(), date=date, description=data[5])
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Событие успешно опубликовано!', reply_markup=markup)
            event = Event.get(Event.date == date)
            users = User.select().where(User.events_notices == True,
                                        User.username != username, User.curr_menu == 'main')
            for user in users:
                bot.send_message(int(user.username), event.title + '\n' +
                                 event.place + '\n' +
                                 event.sporttype.title.capitalize() + '\n' +
                                 event.offsettype.capitalize() + '\n' +
                                 event.date.strftime('%d.%m.%Y %H:%M:%S') + '\n\n' +
                                 event.description,
                                 reply_markup=markup
                                 )
            event.active = False
            event.save()
            return
    if curr_menu == 'event_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        try:
            events = Event.select()
            date = datetime.datetime.strptime(
                message.text.lower(), '%d.%m.%Y %H:%M')
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
        if date in [i.date for i in events]:
            event = Event.get(Event.date == date)
            event.delete_instance()
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Событие от ' +
                             message.text.lower() + ' успешно удалено.', reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
    if curr_menu == 'event_change':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.curr_event_change = datetime.datetime.now()
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        try:
            events = Event.select()
            date = datetime.datetime.strptime(
                message.text.lower(), '%d.%m.%Y %H:%M')
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
        if date in [i.date for i in events]:
            event = Event.get(Event.date == date)
            user_sender.curr_menu = 'event_change_type'
            user_sender.curr_event_change = date
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Вы выбрали событие от ' +
                             date.strftime('%d.%m.%Y %H:%M') + '.\n\nЧто Вы хотие изменить?', reply_markup=markup)
    if curr_menu == 'event_change_type':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        elif message.text.lower() == 'добавить участника':
            user_sender.curr_menu = 'event_change_add'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(
                message.chat.id,
                'Введите участника(команду) события и его результат. Введите Отмена для выхода в предыдущее меню.',
                reply_markup=markup)
        elif message.text.lower() == 'удалить участника':
            event = Event.get(Event.date == user_sender.curr_event_change)
            if event.offsettype == 'командный зачет':
                teams = (Team
                         .select()
                         .join(TeamEvent)
                         .where(TeamEvent.event == event))
                if len(teams) == 0:
                    bot.send_message(message.chat.id, 'Участников еще нет.')
                    return
            if event.offsettype == 'личный зачет':
                students = (Student
                            .select()
                            .join(StudentEvent)
                            .where(StudentEvent.event == event))
                if len(students) == 0:
                    bot.send_message(message.chat.id, 'Участников еще нет.')
                    return
            user_sender.curr_menu = 'event_change_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете участника, которого хотие удалить.', reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'event_change_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.curr_event_change = datetime.datetime.now()
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        event = Event.get(Event.date == user_sender.curr_event_change)
        try:
            if event.offsettype == 'командный зачет':
                team = Team.get(Team.title == message.text)
                event = TeamEvent.get(event=event, team=team)
                event.delete_instance()
            elif event.offsettype == 'личный зачет':
                data = re.split(' ', message.text.lower())
                student = Student.get(Student.group == data[2], Student.surname == data[
                    0], Student.name == data[1])
                event = StudentEvent.get(event=event, student=student)
                event.delete_instance()
            user_sender.curr_menu = 'events'
            user_sender.curr_event_change = datetime.datetime.now()
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Вы успешно удалили участника события', reply_markup=markup)
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
    if curr_menu == 'event_change_add':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        event = Event.get(Event.date == user_sender.curr_event_change)
        data = re.split('\n', message.text)
        if event.offsettype == 'командный зачет':
            if len(data) != 2:
                bot.send_message(message.chat.id, 'Ошибка формата ввода.\n')
                return
            sporttype = SportType.get_or_create(title=event.sporttype.title)
            team = Team.get_or_create(sporttype=sporttype[0], title=data[0])
            member = TeamEvent.get_or_create(
                event=event, team=team[0], defaults={'result': data[1]})
            if not member[1]:
                bot.send_message(
                    message.chat.id, 'Данная комада уже является участником данного события!')
                return
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Вы успешно добавили команду к событию.\nВозврат в предыдущее меню.',
                reply_markup=markup)
            return
        elif event.offsettype == 'личный зачет':
            data = re.split('\n', message.text.lower())
            if len(data) != 6:
                bot.send_message(message.chat.id, 'Ошибка формата ввода.\n')
                return
            student = Student.get_or_create(group=data[4], surname=data[0], name=data[
                1], defaults={'patronymic': data[2], 'faculty': data[3]})
            member = StudentEvent.get_or_create(
                event=event, student=student[0], defaults={'result': data[5]})
            if not member[1]:
                bot.send_message(
                    message.chat.id, 'Данная студент уже является участником данного события!')
                return
            user_sender.curr_menu = 'events'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Вы успешно добавили участника к событию.\nВозврат в предыдущее меню.',
                reply_markup=markup)
            return
    if curr_menu == 'news':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
        elif message.text.lower() == 'месяц':
            user_sender.curr_news = 'month'
            user_sender.save()
            handle_command_news(message)
        elif message.text.lower() == 'год':
            user_sender.curr_news = 'year'
            user_sender.save()
            handle_command_news(message)
        elif message.text.lower() == 'добавить' and user_sender.admin:
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id,
                             'Добавьте названеие и описание новости. Название должно быть уникальным. Введите Отмена для выхода в главное меню.\n\n' +
                             'Например:\n' +
                             'Турнир по волейболу.\n' +
                             'В воскресенье 29 октября в спортзале КСК "Тушино" состоялся турнир на ' +
                             'первенство факультета ИТУ по волейболу (4х4) в зачёт 8-ой спартакиады ' +
                             'ФИТУ по 11 видам программы соревнований с участием команд пяти коллективов ' +
                             'факультета: сборных команд студентов 1 - 4 курсов и МАС (магистрантов, аспирантов, сотрудников).',
                             reply_markup=markup
                             )
        elif message.text.lower() == 'удалить' and user_sender.admin:
            if user_sender.curr_news == 'week':
                delta = datetime.timedelta(days=7)
            elif user_sender.curr_news == 'month':
                delta = datetime.timedelta(days=30)
            elif user_sender.curr_news == 'year':
                delta = datetime.timedelta(days=365)
            begin = datetime.datetime.now()
            end = begin - delta
            news_all = News.select().where(
                News.date <= begin, News.date >= end).order_by(+News.date)
            if len(news_all) == 0:
                bot.send_message(message.chat.id, 'Новостей пока нет.')
                return
            user_sender.curr_menu = 'news_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете новость из списка выше, которую хотие удалить.', reply_markup=markup)
        else:
            data = re.split('\n', message.text)
            if len(data) != 2:
                bot.send_message(
                    message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
                return
            try:
                news = News.get(News.title == data[0])
                bot.send_message(
                    message.chat.id, 'Название новости должно быть уникальным!')
                return
            except:
                news_create = News.create(title=data[0], description=data[
                    1], username=username)
                user_sender.curr_menu = 'main'
                user_sender.save()
                markup = generate_markup(username)
                bot.send_message(
                    message.chat.id, 'Ваша новость успешно опубликована!', reply_markup=markup)
                news = News.get(News.active == True)
                users = User.select().where(User.news_notices == True,
                                            User.username != username, User.curr_menu == 'main')
                for user in users:
                    bot.send_message(int(user.username), 'Свежая новость\n\n' +
                                     news.title + '\n\n' + news.description + '\n\n' + news.date.strftime(
                        '%d.%m.%Y %H:%M:%S'))
                news.active = False
                news.save()
                return
    if curr_menu == 'news_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'news'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
        elif re.match(r'\d+\.\d+\.\d+\s\d+:\d+:\d+', message.text.lower()) is not None:
            date = datetime.datetime.strptime(
                message.text.lower(), '%d.%m.%Y %H:%M:%S')
            news = News.get(News.date >= date)
            news.delete_instance()
            user_sender.curr_menu = 'news'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Новость от ' +
                             message.text.lower() + ' успешно удалена.', reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
    if curr_menu == 'sport_types':
        sport_types = SportType.select().order_by(SportType.title)
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.curr_sporttype = ''
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
            return
        elif message.text.lower() == 'добавить' and user_sender.admin:
            user_sender.curr_menu = 'sport_type_add'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, 'Добавьте вид спорта. Введите Отмена для выхода в предыдущее меню.\n\n' +
                             'Например:\n' +
                             'Баскетбол',
                             reply_markup=markup
                             )
        elif message.text.lower() == 'удалить' and user_sender.admin:
            if len(sport_types) == 1:
                bot.send_message(message.chat.id, 'Видов спорта пока нет.')
                return
            user_sender.curr_menu = 'sport_type_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете вид спорта списка выше, который хотие удалить.', reply_markup=markup)
        elif message.text.lower() in [i.title for i in sport_types]:
            user_sender.curr_sporttype = message.text.lower()
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            sport_type = SportType.get(
                SportType.title == user_sender.curr_sporttype)
            teams = Team.select().where(Team.sporttype == sport_type)
            if len(teams) == 0:
                bot.send_message(
                    message.chat.id, 'Команд пока нет.', reply_markup=markup)
                return
            for team in teams:
                students = (Student
                            .select()
                            .join(StudentTeam)
                            .join(Team)
                            .where(Team.title == team.title))
                students_str = ''
                for student in students:
                    students_str += student.surname.capitalize() + ' ' + student.name.capitalize() + \
                                    ' ' + student.patronymic + ' ' + student.group.upper() + '\n'
                events = (Event
                          .select()
                          .join(TeamEvent)
                          .join(Team)
                          .where(Team.title == team.title))
                events_str = ''
                for event in events:
                    teamevent = TeamEvent.get(
                        TeamEvent.event == event, TeamEvent.team == team)
                    events_str += event.title + '\n' + \
                                  event.date.strftime(
                                      '%d.%m.%Y %H:%M:%S') + '\n' + teamevent.result + '\n'
                notice = team.title.capitalize() + '\n' + students_str + '\n' + events_str
                bot.send_message(message.chat.id, notice,
                                 reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'sport_type_add':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        else:
            sporttype = SportType.get_or_create(title=message.text.lower())
            if not sporttype[1]:
                bot.send_message(
                    message.chat.id, 'Данный вид спорта уже существует!')
                return
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Вы добавили новый вид спорта.', reply_markup=markup)
    if curr_menu == 'sport_type_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        try:
            sporttype = SportType.get(SportType.title == message.text.lower())
            sporttype.delete_instance()
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Вид спорта ' +
                             message.text.lower() + ' успешно удален.', reply_markup=markup)
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'teams':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'sport_types'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
        elif message.text.lower() == 'добавить' and user_sender.admin:
            user_sender.curr_menu = 'team_add'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id,
                             'Добавьте название команды. Название должно быть уникальным. Введите Отмена для выхода в главное меню.\n\n' +
                             'Например:\n' +
                             'Красные орлы',
                             reply_markup=markup
                             )
        elif message.text.lower() == 'удалить' and user_sender.admin:
            sport_type = SportType.get(
                SportType.title == user_sender.curr_sporttype)
            teams = Team.select().where(Team.sporttype == sport_type)
            if len(teams) == 0:
                bot.send_message(message.chat.id, 'Команд пока нет.')
                return
            user_sender.curr_menu = 'team_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете команду из списка выше, которую хотие удалить.', reply_markup=markup)
        elif message.text.lower() == 'изменить' and user_sender.admin:
            sport_type = SportType.get(
                SportType.title == user_sender.curr_sporttype)
            teams = Team.select().where(Team.sporttype == sport_type)
            if len(teams) == 0:
                bot.send_message(message.chat.id, 'Команд пока нет.')
                return
            user_sender.curr_menu = 'team_change'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете команду из списка выше, которую хотие изменить.', reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'team_add':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        else:
            sport_type = SportType.get(
                SportType.title == user_sender.curr_sporttype)
            team = Team.get_or_create(title=message.text, sporttype=sport_type)
            if not team[1]:
                bot.send_message(
                    message.chat.id, 'Данная команда уже существует!')
                return
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Вы добавили новую команду: ' + message.text, reply_markup=markup)
    if curr_menu == 'team_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        try:
            team = Team.get(Team.title == message.text)
            team.delete_instance()
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Команда ' +
                             message.text + ' успешно удалена.', reply_markup=markup)
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
            return
    if curr_menu == 'team_change':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.curr_team = ''
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        try:
            team = Team.get(Team.title == message.text)
            user_sender.curr_menu = 'team_change_type'
            user_sender.curr_team = team.title
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Вы выбрали команду: ' +
                             team.title + '.\n\nЧто Вы хотие изменить?', reply_markup=markup)
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'team_change_type':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        elif message.text.lower() == 'название':
            user_sender.curr_menu = 'team_change_name'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(
                message.chat.id, 'Введите новое название команды. Введите Отмена для выхода в предыдущее меню.',
                reply_markup=markup)
        elif message.text.lower() == 'состав':
            user_sender.curr_menu = 'team_change_member'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете действие.', reply_markup=markup)
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'team_change_name':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        else:
            try:
                team = Team.update(title=message.text).where(
                    Team.title == user_sender.curr_team)
                team.execute()
                user_sender.curr_menu = 'teams'
                user_sender.curr_team = message.text.lower()
                user_sender.save()
                markup = generate_markup(username)
                bot.send_message(
                    message.chat.id, 'Вы успешно изменили название команды. Возврат в предыдущее меню.',
                    reply_markup=markup)
            except:
                bot.send_message(
                    message.chat.id, 'Данная команда уже существует!')
    if curr_menu == 'team_change_member':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        elif message.text.lower() == 'удалить':
            students = (Student
                        .select()
                        .join(StudentTeam)
                        .join(Team)
                        .where(Team.title == user_sender.curr_team))
            if len(students) == 0:
                bot.send_message(message.chat.id, 'Состав пуст.')
                return
            user_sender.curr_menu = 'team_change_member_del'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Выберете члена команды, которого хотите удалить.', reply_markup=markup)
        elif message.text.lower() == 'добавить':
            user_sender.curr_menu = 'team_change_member_add'
            user_sender.save()
            markup = telebot.types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, 'Введите нового члена команды. ' +
                             'При отсутвии какого-либо пункта оставьте строку пустой. ' +
                             'Введите Отмена для выхода в главное меню.\n\n' +
                             'Например:\n' +
                             'Иванов\n' +
                             'Иван\n' +
                             'Иванович\n' +
                             'ИТУ\n' +
                             'Кс-10',
                             reply_markup=markup
                             )
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'team_change_member_del':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        data = re.split(' ', message.text.lower())
        try:
            student = Student.get(Student.group == data[
                2], Student.surname == data[0])

            team = Team.get(Team.title == user_sender.curr_team)
            studentteam = StudentTeam.get(team=team, student=student)
            studentteam.delete_instance()
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Студент успешно удален из команды: ' +
                             user_sender.curr_team + '. Возврат в предыдущее меню.', reply_markup=markup)
        except:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.')
    if curr_menu == 'team_change_member_add':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'teams'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в предыдущее меню.', reply_markup=markup)
            return
        data = re.split('\n', message.text.lower())
        if len(data) != 5:
            bot.send_message(
                message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
            return
        team = Team.get(Team.title == user_sender.curr_team)
        student = Student.get_or_create(
            group=data[4],
            surname=data[0],
            defaults={
                'name': data[1],
                'patronymic': data[2],
                'faculty': data[3]
            }
        )
        studentteam = StudentTeam.get_or_create(team=team, student=student[0])
        user_sender.curr_menu = 'teams'
        user_sender.save()
        markup = generate_markup(username)
        if not studentteam[1]:
            bot.send_message(
                message.chat.id, 'Данный студент уже является членом данной команды!')
            return
        bot.send_message(
            message.chat.id, 'Вы добавили нового члена команды.', reply_markup=markup)
    if curr_menu == 'notices':
        markup = generate_markup(username)
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
            return
        elif message.text.lower() == 'новости':
            user_sender.news_notices = not user_sender.news_notices
        elif message.text.lower() == 'события':
            user_sender.events_notices = not user_sender.events_notices
        elif message.text.lower() == 'запросы администраторов':
            user_sender.admins_notices = not user_sender.admins_notices
        else:
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.', reply_markup=markup)
            return
        user_sender.curr_menu = 'main'
        user_sender.save()
        markup = generate_markup(username)
        bot.send_message(
            message.chat.id, 'Вы успешно изменили статус уведомлений.', reply_markup=markup)
        return
    if curr_menu == 'login':
        if message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
        else:
            data = re.split('\n', message.text.lower())
            if len(data) != 5:
                bot.send_message(
                    message.chat.id, 'Не верный формат.\nПовторите ввод данных или введите Отмена.')
                return
            student = Student.get_or_create(
                group=data[4],
                surname=data[0],
                defaults={
                    'name': data[1],
                    'patronymic': data[2],
                    'faculty': data[3]
                }
            )
            user_sender.student = student[0]
            user_sender.login = True
            user_sender.save()
            name = data[1].capitalize()
            patronymic = data[2].capitalize()
            if patronymic != '':
                patronymic = ' ' + patronymic
            user_sender.curr_menu = 'main'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Добро пожаловать, ' + name + patronymic +
                             '!\nТеперь Вам доступна статистика Ваших спортивных достижений!', reply_markup=markup)
            return
    if curr_menu == 'admin_notice':
        requests = AdminRequest.select().where(
            AdminRequest.active == True).order_by(+AdminRequest.date)
        if len(requests) == 1:
            curr_menu = 'main'
        else:
            curr_menu = 'admin_notices'
        try:
            request = AdminRequest.get(AdminRequest.curr == True)
            curr_menu = 'main'
        except:
            pass
        user_sender.curr_menu = curr_menu
        user_sender.save()
        admin_notice(message)
        return
    if curr_menu == 'admin_notices':
        if message.text.lower() in ('утвердить все', 'отклонить все'):
            requests = AdminRequest.select().where(
                AdminRequest.active == True).order_by(+AdminRequest.date)
            user_sender.curr_menu = 'main'
            user_sender.save()
            for request in requests:
                request.curr = True
                request.save()
                admin_notice(message)
        elif re.match(r'@\w+', message.text.lower()) is not None:
            markup = generate_markup(username)
            try:
                request = AdminRequest.get(
                    AdminRequest.active == True, AdminRequest.nickname == message.text.lower())
            except:
                bot.send_message(
                    message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.', reply_markup=markup)
                return
            request.curr = True
            request.save()
            user_sender.curr_menu = 'admin_notice'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Вы выбрали запрос пользователя: ' +
                             message.text.lower() + '\n' + 'Утвердить запрос?', reply_markup=markup)
        elif re.match(r'\d+', message.text.lower()) is not None:
            markup = generate_markup(username)
            try:
                request = AdminRequest.get(
                    AdminRequest.active == True, AdminRequest.username == message.text.lower())
            except:
                bot.send_message(
                    message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.', reply_markup=markup)
                return
            request.curr = True
            request.save()
            user_sender.curr_menu = 'admin_notice'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(message.chat.id, 'Вы выбрали запрос пользователя: ' +
                             message.text.lower() + '\n' + 'Утвердить запрос?', reply_markup=markup)
        elif message.text.lower() == 'отмена':
            user_sender.curr_menu = 'main'
            user_sender.save()
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Возврат в главное меню.', reply_markup=markup)
        else:
            markup = generate_markup(username)
            bot.send_message(
                message.chat.id, 'Ошибка ввода.\nВоспользуйтесь меню ввода.', reply_markup=markup)


@bot.message_handler(content_types=['document'])
def handle_document(message):
    pass


if __name__ == '__main__':
    if not os.path.exists('data'):
        os.mkdir('data')
        os.mkdir('data/gallery/')
    db.connect()
    db.create_tables([SportType, Team, Student, Event, StudentTeam, StudentEvent,
                      TeamEvent, User, AdminRequest, News, Album, Media], safe=True)
    sporttype = SportType.get_or_create()
    student = Student.get_or_create()
    bot.polling(none_stop=True)
